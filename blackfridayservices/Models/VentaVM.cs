﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blackfridayservices.Models
{
    public class VentaVM
    {
        [JsonProperty("id")]
        public int? Id { get; set; }
        [JsonProperty("tipoFactura")]
        public string TipoFactura { get; set; }
        [JsonProperty("nombre")]
        public string Nombre { get; set; }
        [JsonProperty("nroDocumento")]
        public string NroDocumento { get; set; }
        [JsonProperty("correo")]
        public string Correo { get; set; }
        [JsonProperty("nombreCalle")]
        public string NombreCalle { get; set; }
        [JsonProperty("numCalle")]
        public string NumCalle { get; set; }
        [JsonProperty("ciudad")]
        public string Ciudad { get; set; }
        [JsonProperty("distrito")]
        public string Distrito { get; set; }
        [JsonProperty("telefono")]
        public string Telefono { get; set; }
        [JsonProperty("referencia")]
        public string Referencia { get; set; }
        [JsonProperty("nombreProducto")]
        public string NombreProducto { get; set; }
        [JsonProperty("fechaRegistro")]
        public string FechaRegistro { get; set; }
    }
}