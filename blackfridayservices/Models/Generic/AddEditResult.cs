﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blackfridayservices.Models.Generic
{
    public class AddEditResult
    {
        [JsonProperty("result")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Result Result { get; set; }

        [JsonProperty("id")]
        public int? Id { get; set; }
    }
}