﻿using blackfridayservices.Models.Generic;
using blackfridayservices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace blackfridayservices.Logic
{
    public interface IVentaRepository
    {
        Task<List<VentaVM>> GetVentas();
        Task<AddEditResult> AddEditVenta(AddEditVentaVM model);
    }
}