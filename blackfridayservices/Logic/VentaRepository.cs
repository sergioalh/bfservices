﻿using blackfridayservices.Models.Generic;
using blackfridayservices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using blackfridayservices.DataModel;

namespace blackfridayservices.Logic
{
    public class VentaRepository : IVentaRepository
    {
        private readonly blackfridayEntities _context = new blackfridayEntities();

        public async Task<AddEditResult> AddEditVenta(AddEditVentaVM model)
        {
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var Venta = await _context.venta.FindAsync(model.Id);
                    if (Venta == null)
                    {
                        Venta = new venta
                        {
                            fecharegistro = DateTime.Now.ToString()
                        };
                        _context.venta.Add(Venta);
                    }

                    Venta.nombre = model.Nombre;
                    Venta.ciudad = model.Ciudad;
                    Venta.correo = model.Correo;
                    Venta.distrito = model.Distrito;
                    Venta.nombrecalle = model.NombreCalle;
                    Venta.nombreproducto = model.NombreProducto;
                    Venta.nrodocumento = model.NroDocumento;
                    Venta.numerocalle = model.NumCalle;
                    Venta.referencia = model.Referencia;
                    Venta.telefono = model.Telefono;
                    Venta.tipofactura = model.TipoFactura;

                    await _context.SaveChangesAsync();
                    transaction.Complete();

                    return new AddEditResult
                    {
                        Result = Result.Success,
                        Id = Venta.id
                    };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new AddEditResult
                {
                    Result = Result.Failed
                };
            }
        }

        public async Task<List<VentaVM>> GetVentas()
        {
            try
            {
                var query = _context.venta
                   /* .OrderByDescending(x => x.FechaCreacion)*/.AsQueryable();



                var MotivosList = query.AsEnumerable().Select(x =>
                    new VentaVM
                    {
                        Ciudad = x.ciudad,
                        TipoFactura = x.tipofactura,
                        Correo = x.correo,
                        Distrito = x.distrito,
                        FechaRegistro = x.fecharegistro,
                        Id = x.id,
                        Nombre = x.nombre,
                        NombreCalle = x.nombrecalle,
                        NombreProducto = x.nombreproducto,
                        NroDocumento = x.nrodocumento,
                        NumCalle = x.numerocalle,
                        Referencia = x.referencia,
                        Telefono = x.telefono

                    }).ToList();

                return MotivosList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}