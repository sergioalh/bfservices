﻿using blackfridayservices.Logic;
using blackfridayservices.Models.Generic;
using blackfridayservices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace blackfridayservices.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/venta")]
    public class VentaController : BaseController
    {
        private readonly IVentaRepository _repository;

        public VentaController(IVentaRepository repository)
        {
            _repository = repository;
        }


        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> AddVenta([FromBody] AddEditVentaVM model)
        {
            try
            {
                var result = await _repository.AddEditVenta(model);
                if (result.Result == Result.Failed)
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                    response.Status = "Bad Request";
                    response.Message = "Could not perform the operation";
                    response.Result = result;
                    return new ErrorResult(response, Request);
                }

                response.StatusCode = HttpStatusCode.Created;
                response.Status = "Created";
                response.Result = result;

                return Ok(response);
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Status = "Bad Request";
                response.Message = e.Message;
                return new ErrorResult(response, Request);
            }
        }
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetVentas()
        {
            try
            {
                response.StatusCode = HttpStatusCode.OK;
                response.Status = "OK";
                response.Result = await _repository.GetVentas();

                return Ok(response);
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Status = "Bad Request";
                response.Message = e.Message;

                return new ErrorResult(response, Request);
            }
        }
    }
}
